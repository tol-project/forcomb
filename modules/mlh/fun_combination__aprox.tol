
//////////////////////////////////////////////////////////////////////////////
Real MLH_Trans_Rel_Aprox_MaxIter = 10;
//////////////////////////////////////////////////////////////////////////////
Matrix MLH_Trans_Rel_Aprox
(
  Code t1,      // Matrix(Matrix z1) is a Column vector transformation
  Code t1Inv,   // Matrix(Matrix z1) is a inverse Column vector transformation
  Code t2,      // Matrix(Matrix z2) is a Column vector transformation
  Code t2Inv,   // Matrix(Matrix z2) is a inverse Column vector transformation
  Matrix nuZ1,
  Matrix nuZ2,
  Matrix cov,
  Code rel,     // Matrix(Matrix z1, Set paramSet) rel(z1, paramSet)=z2
  Set paramSet, // Additional parameters for rel relation function between
                // z1 and z2 
  Real show     // If it is TRUE show Marquardt Traces.
)
//////////////////////////////////////////////////////////////////////////////
{
  Real eps = 2**(-20); // ~ 10**(-6)
  Matrix nuZ = nuZ1<<nuZ2;
  // Se obtienen las derivadas de las inversas de las transformaciones
  Matrix nu1 = t1(nuZ1);
  Matrix mEps1 = Zeros(Rows(nu1),1) + eps/2;
  Matrix t1Inv_D1 = (t1Inv(nu1 + mEps1) - t1Inv(nu1 - mEps1)) * 1/eps;
  Matrix nu2 = t2(nuZ2);
  Matrix mEps2 = Zeros(Rows(nu2),1) + eps/2;
  Matrix t2Inv_D1 = (t2Inv(nu2 + mEps2) - t2Inv(nu2 - mEps2)) * 1/eps;
  Matrix tInv_D1_Q = SetDiag(MatSet(Tra(t1Inv_D1<<t2Inv_D1))[1]);
  Matrix covNA = tInv_D1_Q * cov * tInv_D1_Q;
  // Se recupera o aproxima la relaci�n lineal
  Matrix matA = Tra(SetMat(For(1, Rows(nuZ1), Set (Real r) {
    Matrix mEps = Zeros(Rows(nuZ1),1);
    Real PutMatDat(mEps, r, 1, eps/2);
    Matrix imP = rel(nuZ1 + mEps, paramSet);
    Matrix imN = rel(nuZ1 - mEps, paramSet);
    MatSet(Tra(imP-imN)*1/eps)[1]
  })));
  Matrix a = rel(nuZ1, paramSet) - matA*nuZ1;
  Matrix matB = ConcatColumns(matA, Diag(Rows(matA), -1));
  Matrix b = -a;
  //--------------------------------------------------------------------------
  Matrix f(Matrix y) {
    covNA*Tra(matB)*SVDInverse(matB*covNA*Tra(matB))*(b-matB*y)+y
  };
  // La soluci�n linearizada 'sol' puede encontrarse fuera del dominio
  // de la transformaci�n: t = t1<<t2.
  Matrix t(Matrix z) { 
    t1(Sub(z,1,1,Rows(nu1),1))<<t2(Sub(z,Rows(nu1)+1,1,Rows(nu2),1)) 
  };
  // Para intentar evitar que la proyecci�n sobre el hiperplano: matB*z-b==0
  // est� fuera del dominio se incrementa el punto inicial progresivamente
  // hasta encontrar una soluci�n v�lida.
  Matrix sol = f(nuZ);
  Real cont = True;
  Real iter = 0;
  Real While(cont & iter<MLH_Trans_Rel_Aprox_MaxIter, {
    Real iter := iter + 1;
    Matrix err = IfMat(IsUnknown(t(sol)), 1, 0);
    Real If(MatMax(err), {
      Matrix sol := f(nuZ + IfMat(err, -sol, 0));    
    1}, cont := False);
  1});
  Real If(cont, {
    WriteLn("No se ha encontrado una soluci�n v�lida tras "
      <<MLH_Trans_Rel_Aprox_MaxIter<<" iteraciones.", "E");
  1});
  sol
};

//////////////////////////////////////////////////////////////////////////////
