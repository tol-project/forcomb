//////////////////////////////////////////////////////////////////////////////
// Ejemplo: x1+x2+x3=x4
//////////////////////////////////////////////////////////////////////////////

#Require ForComb;
Real ForComb::PublishAPI.Com(?);
Real ForComb::PublishAPI.MLH(?);

#Require MMS;

Real PutRandomSeed(2143);

Real n = 3;
Real sigma2 = 0.01;
Date begin = y2000;
Date end = y2011m12;

Set residuals = For(1, n, Serie(Real k){
  SubSer(Gaussian(0, sigma2, Monthly), y1900, end)
});

Set SetMA  = For(1, n, Polyn (Real k)
{
  RandStationary(1, 1)
});

Set SetARI = For(1, n, Polyn(Real k)
{
  (1-B)*RandStationary(1, 3)
});

Set constants = SetOfReal(4, 5, 3);

Set noises = For(1, n, Serie(Real k){
  SubSer(DifEq(SetMA[k]/SetARI[k], residuals[k], constants[k]), begin, end)
});

Set observaciones = For(1, n, Serie(Real k){
  Serie aux = noises[k];
  Text name = "obser"<<k;
  Eval("Serie "+name+" = Exp(aux)")
});

Serie sumaObs = SetSum(observaciones);
Set Append(observaciones, [[ sumaObs]]);

//////////////////////////////////////////////////////////////////////////////

NameBlock dataSet = MMS::Container::ReplaceDataSet([[
  Text _.name = "DataSet"
]]);

NameBlock dataSet::CreateVariable([[
  Text _.name = "Obs1";
  Text _.type = "Serie";
  Text _.expression = "Serie observaciones[1]"
]]);

NameBlock dataSet::CreateVariable([[
  Text _.name = "Obs2";
  Text _.type = "Serie";
  Text _.expression = "Serie observaciones[2]"
]]);

NameBlock dataSet::CreateVariable([[
  Text _.name = "Obs3";
  Text _.type = "Serie";
  Text _.expression = "Serie observaciones[3]"
]]);

NameBlock dataSet::CreateVariable([[
  Text _.name = "SumaObs";
  Text _.type = "Serie";
  Text _.expression = "Serie observaciones[4]"
]]);


NameBlock model = MMS::Container::ReplaceModel([[
  Text _.name = "Model";
  Set _.dataSets = [["DataSet"]]
]]);

Set EvalSet(model::GetDataSet(?)::GetVariables(?), NameBlock (NameBlock var){
  model::CreateSubmodel([[
    Text _.name = var::GetName(?);
    NameBlock _.output = [[
      Text _.name = var::GetName(?);
      Text _.variable = var::GetName(?);
      Text _.transformationLabel = "BoxCox_0_0"
    ]];
    Text _.type ="Linear";
    Date _.end = end;
    NameBlock _.noise = [[
      Text _.type = "ARIMA";
      Text _.arimaLabel = "P1_3DIF1_0AR0_3MA1_0";
      Real _.sigma = 1
    ]]
  ]])
});

NameBlock estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimation";
  NameBlock _.model = model;
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces = False
  ]]
]]);

Real estimation::Execute(?);

NameBlock forecast = MMS::Container::ReplaceForecast([[
  Text _.name = "Estimation";
  NameBlock _.estimation = estimation;
  Real _.begin = 1;
  Real _.end = 12;
  MMS::@SettingsForecast _.settings = [[
    Text _.mode = "Point";
    Real _.showTraces = False
  ]]
]]);
Real forecast::Execute(?);


/*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*
*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*/

// [1] Residuos de las series
Set setResiduals = For(1, n+1, Serie (Real k){
  (MMS::Container::GetEstimation(1)::GetModel.Results(?)::GetSubmodels(?)[k])
  ::GetResiduals(?)
});

// [2] Previsiones
Set setFor = For(1, n+1, Serie (Real k){
  (MMS::Container::GetForecast(1)::GetModel.Forecast(?)::
  GetSubmodels(?)[k])::GetObservations.Forecast.Median(?)
});

/*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*
 Es importante, a la hora de extraer la previsi�n escoger, la mediana
 de la previsi�n, no la media
 utilizando la funci�n
 GetObservations.Forecast.Median
 La media de la previsi�n se calcula como exp(Y_F+(sigma**2/2))
 La mediana se calcula como exp(Y_F)
 donde Y_F es la previsi�n del output
*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*/

Set setForOutput = For(1, n+1, Serie (Real k){
  (MMS::Container::GetForecast(1)::GetModel.Forecast(?)::
  GetSubmodels(?)[k])::GetOutput.Forecast.Mean(?)
});

// [3] ARIMA
Set setARIMA = For(1, n+1, Set (Real k){
  Set arima = (MMS::Container::GetEstimation(1)::GetModel.Results(?)::
  GetSubmodels(?)[k])::GetARIMA(?);
  SetOfPolyn( ARIMAGetMA(arima), ARIMAGetARI(arima) )
});

Set setMA = Traspose(setARIMA)[1];
Set setARI = Traspose(setARIMA)[2];

Set modelARIMA = EvalSet(setARIMA, Set ( Set arima ){
  PolExpand(arima[1], arima[2])
});

// [4] Transformaciones
Code LogMat = Matrix(Matrix mat){Log(mat)};
Code ExpMat = Matrix(Matrix mat){Exp(mat)};

Set traSet = MLHTransf(LogMat, LogMat);
Set InvTraSet = MLHTransf(ExpMat, ExpMat);

Set transfor = TransVec
(
  Code LogMat,
  Code LogMat,
  Code ExpMat,
  Code ExpMat 
);

// Series (Observaciones y Output)
Set setSeries = For(1, n+1, Serie (Real k){
 (MMS::Container::GetEstimation(1)::GetModel.Results(?)::
  GetSubmodels(?)[k])::GetObservations(?)
});
Set setSeriesOutput = For(1, n+1, Serie (Real k){
 (MMS::Container::GetEstimation(1)::GetModel.Results(?)::
  GetSubmodels(?)[k])::GetOutput(?)
});

// Creamos las matrices de la restricci�n lineal
Set resLin = ComTrivialConstrainToUse(4);
Set resLin_ = ComTrivialConstrain(4);

Set combi4var = ComNoLinSt
(
  resLin,         
  transfor,        
  setSeriesOutput,
  setForOutput,
  setResiduals,
  modelARIMA,  
  12   
);

Matrix covMat = MLH_Cov(setResiduals, Monthly);
Matrix a = SetRow( [[ 0 ]] );
Matrix A = SetRow( SetOfReal(1, 1, 1) );

Set LinTrans = MLHConstrain(A, a); 
Set SetLinTrans = NCopy(12, LinTrans);

Set combinacionSuma4_Com = ComEstimMaxProNoLin (combi4var);

Set combinacionSuma4_MLH = MLH_ARIMA_LinRel
(
  setFor,
  setMA,
  setARI,
  traSet,
  InvTraSet,
  covMat,
  SetLinTrans,
  0
);



