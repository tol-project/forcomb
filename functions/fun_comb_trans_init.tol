
//////////////////////////////////////////////////////////////////////////////
Matrix InitializeLCT(Matrix mean, Matrix cov, Matrix boxcox, 
  Matrix mB, Matrix mC)
//////////////////////////////////////////////////////////////////////////////
{
  /* Se encuentran casos en los que el m�todo InnerPoint es excesivamente
     lento. Por ejemplo con n1==1 y n>>150 y en general cuando n1>1
     Tambi�n se encuentran casso en los que fracasa la obtenci�n del valor
     inicial, existiendo valores posibles.
     Set toma pues siempre como m�todo por defecto, la variante 
     que usa TolIpopt: InitializeLCT_Ipopt.
     Real n = Columns(mB); // 58
     Real n1 = Rows(mB); // 18
     Real do_inner = Case(n1==1, n2<150, n1==2, n2<30, True, False); */
  InitializeLCT_Ipopt(mean, cov, boxcox, mB, mC)
};

//////////////////////////////////////////////////////////////////////////////
Matrix InitializeLCT_InnerPoint(Matrix mean, Matrix cov, Matrix boxcox, 
  Matrix mB, Matrix mC)
//////////////////////////////////////////////////////////////////////////////
{
  // WriteLn("Usando 'InitializeLCT_InnerPoint'");
  // Encontrar un valor de X que satisfaga:  B*invT(X) = C
  // y por tanto:  Z := invT(X) <: [T_min, T_max]
  // Se rotan las matrices de modo que la matriz B rotada (M := B*G)  
  // tenga la forma: M = (M1, 0) con M1 diagonal.
  // De este modo:
  //    W := Inv(G)*Z
  //    B*Z = B*G*W = M*W = M1*W1 = C
  // As� pues:
  //    W1 = Inv(M1) * C
  Real n = Columns(mB);
  Real n1 = Rows(mB);
  Set svd = SVD(Tra(mB)*mB);
  Matrix mG = svd[3];
  Matrix mW1 = GaussInverse(Sub(mB*mG, 1, 1, n1, n1)) * mC;
  // Expresamos la restricci�n:
  //   Z > T_min = BC_Second
  // de la forma:
  //   R*w < RMax
  // donde w es la parte variable de W:  w := W2
  // As� pues:
  //   G * W > BC_Second
  //   G * (W1; w) = ((G11,G12); (G21,G22)) * (W1; w) =
  //     = (G11*W1+G12*w; G21*W1+G22*w) = (G11; G21)*W1 + (G12; G22)*w
  //   (G12; G22) * w > BC_Second - (G11; G21) * W1
  Matrix mG11 = Sub(mG, 1, 1, n1, n1);
  Matrix mG12 = Sub(mG, 1, n1+1, n1, n-n1);
  Matrix mG21 = Sub(mG, n1+1, 1, n-n1, n1);
  Matrix mG22 = Sub(mG, n1+1, n1+1, n-n1, n-n1);
  // Si la transformaci�n es identidad no hay problemas 
  // de restricci�n de dominio, de modo que se eliminan dichas restricciones.
  // Se localizan las filas que no necesitan restricci�n:
  Matrix bcA = SubCol(boxcox,[[1]]);
  Matrix bcB = SubCol(boxcox,[[2]]);
  Set cnstRows = NotNullRows(NE(bcA,1));
  If(Card(cnstRows)==0, {
    MatBoxCox(mG * (mW1<<Zeros(n-n1,1)), bcA, bcB)
  }, {
    // se anulan los valores peque�os para evitar el fallo de InnerPoint
    Matrix mR = - Chop(SubRow(mG12<<mG22, cnstRows));
    Matrix mRMax = Chop(SubRow((mG11<<mG21) * mW1 - bcB, cnstRows));
    Set inn = InnerPoint(mR, mRMax);
    Real status = Rows(inn[2]) | Rows(inn[3]) | Rows(inn[4]);
    If(status, {
      Real test = MatMin(LT(mR*inn[1], mRMax));
      If(test, MatBoxCox(mG * (mW1<<inn[1]), bcA, bcB), {
        WriteLn("No parece haber una soluci�n v�lida.", "E");
        If(False, ?)
      })
    }, {
      WriteLn("No se encuentra una soluci�n v�lida.", "E");
      If(False, ?)
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
