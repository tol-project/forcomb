
//////////////////////////////////////////////////////////////////////////////
// TCF: Combinación temporal lineal de previsiones
//      [Time linear Combination of Forecasts]
// T(X_i_t - F_i_t) ~ N(Mean_i_t, Cov_i_t)  ,,  t <: D_i
// B * X = C  para t <: D[t1, t2]  ,,  D_i <= D
// 
// + Se admite (por ahora) que solo hay una restricción temporal,
//   dicho de otro modo, las restricciones son sobre el mismo fechado.
// + Se admiten que las previsiones on independientes.

//////////////////////////////////////////////////////////////////////////////
Set SolveTCF_ByDate(Set forecasts, NameBlock constraint, NameBlock options)
//////////////////////////////////////////////////////////////////////////////
{
  // [Interval]
  // Se establece el intervalo de referencia en los objetos forecast
  NameBlock interval = constraint::GetInterval(?);
  Set EvalSet(forecasts, Real (NameBlock forecast) {
    forecast::SetIntervalReference(interval)
  });
  // [Forecast Info]
  Matrix mBaseCovExt = ObtainBaseCovTCF(forecasts, constraint, options);
  Matrix mPsi = SetConcatDiag(EvalSet(forecasts, 
    Matrix (NameBlock forecast) { forecast::GetPsi(?) }));
  Matrix mCov = mPsi * mBaseCovExt * Tra(mPsi);
  // [Linear Constraint]
  Matrix mB = constraint::GetMatrixB(?);
  Matrix mCx = constraint::GetMatrixC(?);
  // [Combination by Date]
  Set sol_j = Traspose(For(1, interval::GetLength(?), Set (Real t) {
    Date dt = interval::GetDate(t);
    Set set_j = Traspose(For(1, Card(forecasts), Set (Real j) {
      NameBlock forecast = forecasts[j];
      Matrix mMean_tj = forecast::GetMean_AtDate(dt); // column
      Matrix mInd_tj = forecast::GetIndicator(dt);
      Matrix mBCA_tj = forecast::GetBCA_AtDate(dt); // column
      Matrix ones = forecast::GetIndicator_AtDate(dt); // row
      Matrix mB_tj = SubCol(mB, [[j]])*ones;
      [[mMean_tj, mInd_tj, mBCA_tj, mB_tj]]
    }));
    // [Forecast Info]
    Matrix mMean_t = Group("ConcatRows", set_j[1]);
    Real acc = 0;
    Set indices = SetConcat(EvalSet(set_j[2], Set (Matrix mInd) {
      Set inds = NotNullColumns(mInd);
      Set indices_j = EvalSet(inds, Real (Real i) { i + acc });
      Real acc := acc + Columns(mInd);
      indices_j
    }));
    Matrix mCov_t = SubRow(SubCol(mCov, indices), indices);
    Matrix mBCA_t = Group("ConcatRows", set_j[3]);
    // [Linear Constraint]
    Matrix mB_t = Group("ConcatColumns", set_j[4]);
    Matrix mC_t = SubCol(mCx, [[If(Columns(mCx)==1, 1, t)]]);
    // [Forecast Combination]
    Matrix mSol_t = SolveLCT_Fixing(mMean_t, mCov_t, mBCA_t, mB_t, mC_t);
    Matrix If(Not(ObjectExist("Matrix", "mSol_t")), mSol_t = mMean_t * ?);
    DeepCopy(set_j[1], mSol_t)
  }));
  Set solutions = EvalSet(sol_j, Matrix (Set s) { Group("ConcatRows", s) });
  _SolveTCF.Return(forecasts, solutions, options)
};

//////////////////////////////////////////////////////////////////////////////
