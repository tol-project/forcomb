
//////////////////////////////////////////////////////////////////////////////
// TCF: Combinación temporal lineal de previsiones
//      [Time linear Combination of Forecasts]
// T(X_i_t - F_i_t) ~ N(Mean_i_t, Cov_i_t)  ,,  t <: D_i
// B * X = C  para t <: D[t1, t2]  ,,  D_i <= D
// 
// + Se admite (por ahora) que solo hay una restricción temporal,
//   dicho de otro modo, las restricciones son sobre el mismo fechado.
// + Se admiten que las previsiones on independientes.

//////////////////////////////////////////////////////////////////////////////
Set SolveTCF_Sequential(Set forecasts, NameBlock constraint, 
  NameBlock options)
//////////////////////////////////////////////////////////////////////////////
{
  // [Interval]
  // Se establece el intervalo de referencia en los objetos forecast
  NameBlock interval = constraint::GetInterval(?);
  Set EvalSet(forecasts, Real (NameBlock forecast) {
    forecast::SetIntervalReference(interval)
  });
  Matrix mB = constraint::GetMatrixB(?);
  Matrix mCx = constraint::GetMatrixC(?);
  // [Combination by Date]
  Set sol_j = Traspose(For(1, interval::GetLength(?), Set (Real t) {
    Date dt = interval::GetDate(t);
    Set set_j = Traspose(For(1, Card(forecasts), Set (Real j) {
      NameBlock forecast = forecasts[j];
      Matrix mMeanC_tj = forecast::GetMeanC_AtDate(dt); // column
      Matrix mPsiC_tj = forecast::GetPsiC_AtDate(dt);
      Matrix mBCA_tj = forecast::GetBCA_AtDate(dt); // column
      Matrix ones = forecast::GetIndicator_AtDate(dt); // row
      Matrix mB_tj = SubCol(mB, [[j]])*ones;
      [[mMeanC_tj, mPsiC_tj, mBCA_tj, mB_tj]]
    }));
    // [Forecast Info]
    Matrix mMean_t = Group("ConcatRows", set_j[1]);
    Matrix mBaseCov_t = ObtainBaseCovTCF_BetweenDates(forecasts, constraint, 
      options, dt, dt);
    Matrix mPsi_t = SetConcatDiag(set_j[2]);
    Matrix mCov_t = mPsi_t * mBaseCov_t * Tra(mPsi_t);
    Matrix mBCA_t = Group("ConcatRows", set_j[3]);
    // [Linear Constraint]
    Matrix mB_t = Group("ConcatColumns", set_j[4]);
    Matrix mC_t = SubCol(mCx, [[If(Columns(mCx)==1, 1, t)]]);
    // [Forecast Combination]
    Matrix mSol_t = SolveLCT_Fixing(mMean_t, mCov_t, mBCA_t, mB_t, mC_t);
    Matrix If(Not(ObjectExist("Matrix", "mSol_t")), mSol_t = mMean_t * ?);
    Set sSol_t = DeepCopy(set_j[1], mSol_t);
    Set For(1, Card(forecasts), Real (Real j) {
      NameBlock forecast = forecasts[j];
      Real forecast::SetCombinated_AtDate(sSol_t[j], dt);
    1});
    sSol_t 
  }));
  Set solutions = EvalSet(sol_j, Matrix (Set s) { Group("ConcatRows", s) });
  _SolveTCF.Return(forecasts, solutions, options)
};

//////////////////////////////////////////////////////////////////////////////
